/***************************************************************************** 
 * Project: RooFit                                                           * 
 *                                                                           * 
 * This code was autogenerated by RooClassFactory                            * 
 *****************************************************************************/ 

// Your description goes here... 

#include "Riostream.h" 

#include "Mybkg.h" 
#include "RooAbsReal.h" 
#include "RooAbsCategory.h" 
#include <math.h> 
#include "TMath.h" 

ClassImp(Mybkg); 

 Mybkg::Mybkg(const char *name, const char *title, 
                        RooAbsReal& _x,
                        RooAbsReal& _m0,
                        RooAbsReal& _a1,
                        RooAbsReal& _a2,
                        RooAbsReal& _b0,
                        RooAbsReal& _b1,
                        RooAbsReal& _b2,
                        RooAbsReal& _m1,
                        RooAbsReal& _m2) :
   RooAbsPdf(name,title), 
   x("x","x",this,_x),
   m0("m0","m0",this,_m0),
   a1("a1","a1",this,_a1),
   a2("a2","a2",this,_a2),
   b0("b0","b0",this,_b0),
   b1("b1","b1",this,_b1),
   b2("b2","b2",this,_b2),
   m1("m1","m1",this,_m1),
   m2("m2","m2",this,_m2)
 { 
 } 


 Mybkg::Mybkg(const Mybkg& other, const char* name) :  
   RooAbsPdf(other,name), 
   x("x",this,other.x),
   m0("m0",this,other.m0),
   a1("a1",this,other.a1),
   a2("a2",this,other.a2),
   b0("b0",this,other.b0),
   b1("b1",this,other.b1),
   b2("b2",this,other.b2),
   m1("m1",this,other.m1),
   m2("m2",this,other.m2)
 { 
 } 



 Double_t Mybkg::evaluate() const 
 { 
   // ENTER EXPRESSION IN TERMS OF VARIABLE ARGUMENTS HERE 
   //return x*x;
   if(x<m0) 
   {
       //printf("herehereherehereherehere\n %lf", (pow((x*x-pow(m1+m2,2.))*(x*x-pow(m1-m2,2.)), 0.5)/2.*x)*exp(a1*x+a2*x*x) );
       return (sqrt((x*x-pow(m1+m2,2.))*(x*x-pow(m1-m2,2.)))/2.*x)* exp(a1*x+a2*x*x); 
   }
   else 
   {
       return (sqrt((x*x-pow(m1+m2,2.))*(x*x-pow(m1-m2,2.)))/2.*x)* exp( ((a1*m0+a2*m0*m0)-(b1*m0+b2*m0*m0)) +b1*x+b2*x*x); 
   }
 } 



