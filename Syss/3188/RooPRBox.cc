/**
  * @file RooPRBox.cc
  * @version 1.0
  * @author Marian Stahl
  * @date  2019-11-16
  */

#include "RooPRBox.h"
#include "RooHelpers.h"

ClassImp(RooPRBox)

RooPRBox::RooPRBox(const char* name, const char* title, RooAbsReal& _x, RooAbsReal& _M, RooAbsReal& _m12, RooAbsReal& _m1,
                   RooAbsReal& _m2, RooAbsReal& _m3, RooAbsReal& _slope) :
    RooAbsPdf(name, title), x("x", "Observable", this, _x),
    M("M", "Mass of decaying particle", this, _M),
    m12("m12", "Mass of (12) resonance", this, _m12),
    m1("m1", "Mass of missing particle", this, _m1),
    m2("m2", "Mass of 2nd daughter", this, _m2),
    m3("m3", "Mass of 3rd daughter", this, _m3),
    slope("slope", "slope", this, _slope)
{
#if ROOT_VERSION_CODE > ROOT_VERSION(6,19,0)
    RooHelpers::checkRangeOfParameters(this, { &_slope }, -1., 1., false, " slope should be within [-1,1]");
#endif
}

RooPRBox::RooPRBox(const RooPRBox& other, const char* name) :
    RooAbsPdf(other, name), x("x", this, other.x), M("M", this, other.M), m12("m12", this, other.m12), m1("m1", this, other.m1), m2("m2", this, other.m2), m3("m3", this, other.m3), slope("slope", this, other.slope)
{
}

Double_t RooPRBox::evaluate() const
{
    auto const E2Star = (m12 * m12 - m1 * m1 + m2 * m2) / (2 * m12); //energy of 2 in the 12 RF
    auto const E3Star = (M * M - m12 * m12 - m3 * m3) / (2 * m12); //energy of 3 in the 12 RF
    auto const SumE = E2Star + E3Star;
    auto const term1 = SumE * SumE;
    auto const term2 = std::sqrt(E2Star * E2Star - m2 * m2);
    auto const term3 = std::sqrt(E3Star * E3Star - m3 * m3);
    auto const t2mt3 = term2 - term3;
    auto const t2pt3 = term2 + term3;
    auto const m23_max = std::sqrt(term1 - t2mt3 * t2mt3);
    auto const m23_min = std::sqrt(term1 - t2pt3 * t2pt3);
    return (m23_min <= x && x <= m23_max) ? slope * (x - m23_min) / (m23_max - m23_min) - (slope - 1) / 2. : 0.;
}
